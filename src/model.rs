use std::{
    fs::File,
    io::BufReader,
    ops::{Add, AddAssign, Div},
    path::Path,
};

use glium::implement_vertex;
use itertools::Itertools;
use nom_stl::Mesh;

pub fn read_stl(path: impl AsRef<Path>) -> Mesh {
    let file = File::open(path.as_ref()).unwrap();
    let mut root_vase = BufReader::new(&file);
    nom_stl::parse_stl(&mut root_vase).unwrap()
}

// TODO vectors from nalgebra or similar
#[derive(Copy, Clone, Debug)]
pub struct Vertex3 {
    position: (f32, f32, f32),
}

implement_vertex!(Vertex3, position);

impl Vertex3 {
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        Self {
            position: (x, y, z),
        }
    }
}

impl From<[f32; 3]> for Vertex3 {
    fn from(arr: [f32; 3]) -> Self {
        Self {
            position: (arr[0], arr[1], arr[2]),
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Normal {
    normal: (f32, f32, f32),
}

implement_vertex!(Normal, normal);

impl Normal {
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        Self { normal: (x, y, z) }
    }
}

impl Add for Normal {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        Self {
            normal: (
                self.normal.0 + rhs.normal.0,
                self.normal.1 + rhs.normal.1,
                self.normal.2 + rhs.normal.2,
            ),
        }
    }
}

impl AddAssign for Normal {
    fn add_assign(&mut self, rhs: Self) {
        *self = *self + rhs
    }
}

impl Div<f32> for Normal {
    type Output = Self;
    fn div(self, rhs: f32) -> Self {
        Self {
            normal: (
                self.normal.0 / rhs,
                self.normal.1 / rhs,
                self.normal.2 / rhs,
            ),
        }
    }
}

impl From<[f32; 3]> for Normal {
    fn from(arr: [f32; 3]) -> Self {
        Self {
            normal: (arr[0], arr[1], arr[2]),
        }
    }
}

impl Normal {
    fn normalize(self) -> Self {
        let norm =
            (self.normal.0.powf(2.0) + self.normal.1.powf(2.0) + self.normal.2.powf(2.0)).sqrt();
        self / norm
    }
}

const ACCURACY: f32 = 1000.0;

#[inline(always)]
fn vertex_eq(left: &Vertex3, right: &[f32; 3]) -> bool {
    (left.position.0 * ACCURACY) as i32 == (right[0] * ACCURACY) as i32
        && (left.position.1 * ACCURACY) as i32 == (right[1] * ACCURACY) as i32
        && (left.position.2 * ACCURACY) as i32 == (right[2] * ACCURACY) as i32
}

pub fn decompose(mesh: Mesh) -> (Vec<Vertex3>, Vec<Normal>, Vec<u16>) {
    let triangles = mesh.triangles();

    // Over-estimate the capacity required to load all unique vertices. Not good for gigabyte large models btw
    let mut buffer: Vec<(Vertex3, Normal)> = Vec::with_capacity(triangles.len() * 3);
    let mut indices: Vec<u16> = Vec::with_capacity(triangles.len() * 3);

    for triangle in triangles {
        for vertex in triangle.vertices().iter() {
            // dbg!(triangle, vertex);
            if let Some((i, vn)) = buffer
                .iter_mut()
                .enumerate()
                .find(|(_, (oldv, _))| vertex_eq(oldv, vertex))
            {
                // Already seen vertex
                // Add normal of the triangle face to the vertex's normal.
                vn.1 += triangle.normal().into();
                indices.push(i as u16);
            } else {
                // Unseen vertex
                let v = (*vertex).into();
                let n = triangle.normal().into();
                let i = buffer.len();
                buffer.push((v, n));
                // Now that the vertex is in the buffer, we can take its index and push it to indices
                indices.push(i as u16);
            }
        }
    }
    for (_, normal) in buffer.iter_mut() {
        // Normalize averages the value over all faces
        *normal = normal.normalize();
    }

    let (vertices, normals) = buffer.into_iter().multiunzip();
    (vertices, normals, indices)
}
