#version 140

in vec3 position;
in vec3 normal;

out vec3 v_normal;
out vec3 v_position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 perspective;

void main() {
  mat4 modelview = view * model;
  // https://stackoverflow.com/q/13654401
  v_normal = transpose(inverse(mat3(modelview))) * normal;
  
  gl_Position = perspective * modelview * vec4(position, 1.0);

  v_position = gl_Position.xyz / gl_Position.w;
}
