use std::f32::consts::PI;
use std::time::{Duration, Instant};

use glium::{
    glutin::{self, dpi::PhysicalSize},
    index::PrimitiveType,
    uniform, BackfaceCullingMode, Display, DrawParameters, IndexBuffer, Program, Surface,
    VertexBuffer,
};
use glutin::{
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
    ContextBuilder,
};
use model::{decompose, read_stl};

mod model;

const VERTEX_SHADER_SRC: &str = include_str!("vertex.glsl");

const FRAGMENT_SHADER_SRC: &str = include_str!("fragment.glsl");

fn view_matrix(position: &[f32; 3], direction: &[f32; 3], up: &[f32; 3]) -> [[f32; 4]; 4] {
    let f = {
        let f = direction;
        let len = f[0] * f[0] + f[1] * f[1] + f[2] * f[2];
        let len = len.sqrt();
        [f[0] / len, f[1] / len, f[2] / len]
    };

    let s = [
        up[1] * f[2] - up[2] * f[1],
        up[2] * f[0] - up[0] * f[2],
        up[0] * f[1] - up[1] * f[0],
    ];

    let s_norm = {
        let len = s[0] * s[0] + s[1] * s[1] + s[2] * s[2];
        let len = len.sqrt();
        [s[0] / len, s[1] / len, s[2] / len]
    };

    let u = [
        f[1] * s_norm[2] - f[2] * s_norm[1],
        f[2] * s_norm[0] - f[0] * s_norm[2],
        f[0] * s_norm[1] - f[1] * s_norm[0],
    ];

    let p = [
        -position[0] * s_norm[0] - position[1] * s_norm[1] - position[2] * s_norm[2],
        -position[0] * u[0] - position[1] * u[1] - position[2] * u[2],
        -position[0] * f[0] - position[1] * f[1] - position[2] * f[2],
    ];

    [
        [s_norm[0], u[0], f[0], 0.0],
        [s_norm[1], u[1], f[1], 0.0],
        [s_norm[2], u[2], f[2], 0.0],
        [p[0], p[1], p[2], 1.0],
    ]
}

fn main() {
    let event_loop = EventLoop::new();
    let wb = WindowBuilder::new();
    let cb = ContextBuilder::new().with_depth_buffer(24);
    let display = Display::new(wb, cb, &event_loop).unwrap();

    let program =
        Program::from_source(&display, VERTEX_SHADER_SRC, FRAGMENT_SHADER_SRC, None).unwrap();

    let mut t: f32 = PI;
    let mesh = read_stl("monke.stl");
    let (vertices, normals, indices) = decompose(mesh);
    let vertices = VertexBuffer::new(&display, &vertices).unwrap();
    let normals = VertexBuffer::new(&display, &normals).unwrap();
    let indices = IndexBuffer::new(&display, PrimitiveType::TrianglesList, &indices).unwrap();

    let mut mouse_pos = [0.0, 0.0f32];
    event_loop.run(move |ev, _, control_flow| {
        let next_frame_time = Instant::now() + Duration::from_nanos(16_666_666);
        *control_flow = ControlFlow::WaitUntil(next_frame_time);
        match ev {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => *control_flow = ControlFlow::Exit,
            Event::WindowEvent {
                event: WindowEvent::CursorMoved { position, .. },
                ..
            } => {
                let PhysicalSize { height, width } = display.gl_window().window().inner_size();
                mouse_pos[0] = (position.x as f32 / width as f32 - 0.5) * PI;
                mouse_pos[1] = (position.y as f32 / height as f32 - 0.5) * PI;
            }
            _ => (),
        }

        // t += 0.002;

        let light = [0.0, 0.0, -1.0f32];

        let mut target = display.draw();

        target.clear_color_and_depth((0.0, 0.0, 0.0, 1.0), 1.0);

        let perspective = {
            let (width, height) = target.get_dimensions();
            let aspect_ratio = height as f32 / width as f32;

            let fov = PI / 3.0;
            let zfar = 1024.0;
            let znear = 0.1;

            let f = 1.0 / (fov / 2.0).tan();

            [
                [f * aspect_ratio, 0.0, 0.0, 0.0],
                [0.0, f, 0.0, 0.0],
                [0.0, 0.0, (zfar + znear) / (zfar - znear), 1.0],
                [0.0, 0.0, -(2.0 * zfar * znear) / (zfar - znear), 0.0],
            ]
        };

        let view = view_matrix(
            &[
                -mouse_pos[0].sin(),
                mouse_pos[1].sin() * mouse_pos[0].cos(),
                -mouse_pos[0].cos() * mouse_pos[1].cos(),
            ],
            &[
                mouse_pos[0].sin(),
                -mouse_pos[1].sin() * mouse_pos[0].cos(),
                mouse_pos[0].cos() * mouse_pos[1].cos(),
            ],
            &[0.0, 1.0, 0.0],
        );

        let uniforms = uniform! {
            model: [
                [t.cos(), 0.0, t.sin(), 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [t.sin(), 0.0, -t.cos(), 0.0],
                [0.0, 0.0, 0.0, 1.0f32],
            ],
            view: view,
            perspective: perspective,
            u_light: light
        };

        let params = DrawParameters {
            depth: glium::Depth {
                test: glium::DepthTest::IfLess,
                write: true,
                ..Default::default()
            },
            // Discard all triangles that are clockwise as blender
            // exports STLs with all triangles arranged counter
            // clockwise when viewed from outside. Culling clockwise
            // avoids drawing these internal facing triangles.
            backface_culling: BackfaceCullingMode::CullClockwise,
            ..Default::default()
        };

        target
            .draw(
                (&vertices, &normals),
                &indices,
                &program,
                &uniforms,
                &params,
            )
            .unwrap();
        target.finish().unwrap();
    });
}
